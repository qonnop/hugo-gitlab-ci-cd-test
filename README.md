# GitLab CI/CD Test Repository

This project is for testing the GitLab infrastructure for [hugo](https://gohugo.io/). See its build on [gitlab.io](https://qonnop.gitlab.io/hugo-gitlab-ci-cd-test/).
